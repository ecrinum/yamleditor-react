let people = [
  {label:'forname',type:'string'},
  {label:'surname',type:'string'},
  {label:'gender',type:'string'},
  {label:'orcid',type:'string'},
  {label:'viaf',type:'string'},
  {label:'foaf',type:'string'},
  {label:'isni',type:'string'}
];
let lang = [
  {label:'fr',value:'fr'},
  {label:'en',value:'en'},
  {label:'it',value:'it'},
  {label:'es',value:'es'},
  {label:'pt',value:'pt'},
  {label:'de',value:'de'},
  {label:'uk',value:'uk'},
  {label:'ar',value:'ar'}
]

let schema = [
  {label:'title_f', type: 'string',control:[
    {label:'title',rule:'stripMarkdown'}
  ]},
  {label:'subtitle_f', type: 'string',control:[
    {label:'subtitle',rule:'stripMarkdown'}
  ]},
  {label:'authors', type:'array',of:people},
  {label:'date', type: 'date',control:[
    {label:'year',rule:'split',delimiter:'/',group:0},
    {label:'month',rule:'split',delimiter:'/',group:1},
    {label:'day',rule:'split',delimiter:'/',group:2},
  ]},
  {label:'abstract', type:'array',of:[ // mettre au pluriel pour un array
    {label:'text_f',type:'text',control:[
      {label:'text',rule:'stripMarkdown'}
    ]},
    {label:'lang',type:'string',in:lang}
  ]},
  {label:'keyword_fr_f',type: 'string',control:[ // transformer en array
    {label:'keyword_fr',rule:'stripMarkdown'}
  ]},
  {label:'keyword_en_f',type: 'string',control:[  // transformer en array of strings
    {label:'keyword_en',rule:'stripMarkdown'}
  ]},
  {label:'controlledKeywords',type:'array',of: [
    {label:'label',type:,'string'},
    {label:'idRameau', type:'string'},
    {label:'uriRameau', type:'string'}
  ]},
  {label:'lang',type:'string',in:lang},
  {label:'id', type: 'string'}, // rendu generique (id_sp)
  {label:'url_article', type: 'string'}, // rendu generique (url_article_sp)
  {label:'typeArticle',type:'array',of:'string'}, // categories
  {label:'publisher', type: 'string'},
  {label:'journal', type: 'string'},
  {label:'issnnum', type: 'string'},
  {label:'directors', type:'array',of:people},// mis au pluriel pour un array
  {label:'prod', type: 'string'},
  {label:'prodnum', type: 'string'},
  {label:'diffnum', type: 'string'},
  {label:'rights', type: 'string'},
  {label:'dossier', type: 'array', of:[ // mettre au pluriel pour un array
    {label:'title', type:'string'},
    {label:'id', type:'string'}
  ]},
  {label:'redacteurDossier',type:'array',of:people}, //Mettre au pluriel pour un array + anglicisé (issueDirectors) ?
  {label:'reviewers',type:'array',of:people},
  {label:'translator',type:'array',of:people}, // mettre au pluriel pour un array
  {label:'transcriber',type:'array',of:people}, // ajouté
  {label:'translationOf',type:'array',of:[
    {label:'lang',type:'string',in:lang},
    {label:'title',type:'string'}, // anglicisé
    {label:'url',type:'string'}
  ]},
  {label:'articleslies',type:'array',of:[
    {label:'url',type:'string'},
    {label:'title',type:'string'}, // anglicisé
    {label:'authors',type:'array',of:people} // modifié vers un "array of people" + anglicisé
  ]},
  {label:'bibliography', type: 'string'},
  {label:'link-citations', type: 'boolean', in:[{label:'true',value:true},{label:"false",value:false}]}, // si possible le yamlEditor explicitera les deux options
  {label:'nocite',type:'string',in:[{label:'Only citation(s) used',value:''},{label:"all citations",value:'@*'}]} // si possible le yamlEditor explicitera les deux options
]
