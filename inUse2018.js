let schema = [
  {label:'id_sp', type: 'string',control:[ // rendre generique
    {label:'bibliography', type: 'string'}
  ]}, 
  {label:'title_f', type: 'string',control:[
    {label:'title',rule:'stripMarkdown'}
  ]},
  {label:'subtitle_f', type: 'string',control:[
    {label:'subtitle',rule:'stripMarkdown'}
  ]},
  {label:'date', type: 'date',control:[
    {label:'year',rule:'split',delimiter:'/',group:0},
    {label:'month',rule:'split',delimiter:'/',group:1},
    {label:'day',rule:'split',delimiter:'/',group:2},
  ]},
  {label:'url_article_sp', type: 'string'}, // rendre generique
  {label:'publisher', type: 'string'},
  {label:'prod', type: 'string'},
  {label:'prodnum', type: 'string'},
  {label:'diffnum', type: 'string'},
  {label:'rights', type: 'string'},
  {label:'issnnum', type: 'string'},
  {label:'journal', type: 'string'},
  {label:'director', type:'array',of:'people'},// mettre au pluriel pour un array
  {label:'abstract', type:'array',of:[ // mettre au pluriel pour un array
    {label:'text_f',type:'text',control:[
      {label:'text',rule:'stripMarkdown'}
    ]},
    {label:'lang',type:'string',in:['fr','en','ita','es','pt','de','uk','ar']}
  ]},
  {label:'authors', type:'array',of:'people'},
  {label:'dossier', type: 'array', of:[ // mettre au pluriel pour un array
    {label:'title', type:'string'},
    {label:'id', type:'string'}
  ]},
  {label:'redacteurDossier',type:'array',of:'people'}, //Mettre au pluriel pour un array
  {label:'typeArticle',type:'array',of:'string'}, // categories
  {label:'translator',type:'array',of:'people'}, // mettre au pluriel pour un array
  {label:'lang',type:'string',in:['fr','en','ita','es','pt','de','uk','ar']},
  {label:'orig_lang',type:'string',in:['fr','en','ita','es','pt','de','uk','ar']},
  {label:'translations',type:'array',of:[
    {label:'lang',type:'string',in:['fr','en','ita','es','pt','de','uk','ar']},
    {label:'titre',type:'string'},
    {label:'url',type:'string'}
  ]},
  {label:'articleslies',type:'array',of:[
    {label:'url',type:'string'},
    {label:'titre',type:'string'},
    {label:'auteur',type:'string'} // modifier vers un "array of people"
  ]},
  {label:'reviewers',type:'array',of:'people'},
  {label:'keyword_fr_f',type: 'string',control:[ // transformer en array
    {label:'keyword_fr',rule:'stripMarkdown'}
  ]},
  {label:'keyword_en_f',type: 'string',control:[  // transformer en array of strings
    {label:'keyword_en',rule:'stripMarkdown'}
  ]},
  {label:'controlledKeywords',type:'array',of: [
    {label:'label',type:,'string'},
    {label:'idRameau', type:'string'},
    {label:'uriRameau', type:'string'}
  ]},
  {label:'link-citations', type: 'boolean'},
  {label:'nocite',type:'string',in:['','@*']}
]
